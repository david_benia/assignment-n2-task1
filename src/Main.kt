import Point.*

fun main() {
    val p1 = Point(4.2, 6.9)
    val p2 = Point(12.7, 7.4)
    val p3 = Point(4.2, 6.9)

    println("$p1\n$p2\n$p3\n")
    println("p1 = p2 -> ${p1.equals(p2)}")
    println("p1 = p3 -> ${p1.equals(p3)}\n")

    val mirrorP1_X = p1.mirrorTranslate(true)
    val mirrorP1_Y = p1.mirrorTranslate(false)

    println("Mirrored p1 across each axis:\n${mirrorP1_X}\n${mirrorP1_Y}\n")

    println("Distance between p1 and p2 = " + "%.2f".format(p1.distanceBetween(p2)))
}
