package Point

class Point(private val x: Double = 0.0, private val y: Double = 0.0) {

    override fun toString(): String {
        return "Point '${this.hashCode()}' - {X: $x ; Y: $y}"
    }

    fun equals(other: Point): Boolean {
        return (this.x == other.x && this.y == other.y)
    }

    fun mirrorTranslate(axis: Boolean): Point {
        if(axis){
            return Point((this.x?.unaryMinus()), this.y)
        } else {
            return Point(this.x, (this.y?.unaryMinus()))
        }
    }

    fun distanceBetween(other: Point): Double {
        val horizontal = Math.abs(this.x - other.x)
        val vertical = Math.abs(this.y - other.y)

        return Math.sqrt(Math.pow(horizontal, 2.0) + Math.pow(vertical, 2.0))

    }
}